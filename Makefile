
remove-30d-old-bing-wallpapers:
	@find ~/Pictures/BingWallpaper -mtime +30 -exec rm '{}' +
.PHONY: cleanup-old-bing-wallpapers

.PHONY: bootstrap
bootstrap:
	@sudo dnf install -y \
		ansible \
		ansible-collection-community-general

vm_provider ?= libvirt
# Set this flag to continue the last provision. Then, no new VM is recreated.
continue_last =
# Run tasks with these tags. By default, run all tasks.
run_tags ?=

.PHONY: test-provision
test-provision:
	@if [ -n "$(continue_last)" ]; then \
		TAGS="$(run_tags)" vagrant provision; \
	else \
		machine_state=$(shell vagrant status --machine-readable | grep ",state," | cut -d',' -f4); \
		if [ "${machine_state}" != "not_created" ]; then \
			vagrant halt || : ; \
			vagrant destroy --force; \
		fi; \
		TAGS="$(run_tags)" vagrant up --provider $(vm_provider) --no-destroy-on-error; \
	fi

extra_args =

.PHONY: provision-this-host
# Refer to: https://www.middlewareinventory.com/blog/run-ansible-playbook-locally/
provision-this-host:
	@if [ ! -e site.yaml ]; then \
		echo "Missing site.yaml which provides the become password."; \
		false; \
	fi
	@if [ ! -e vault-pass.txt ]; then \
		echo "Missing vault-pass.txt to decrypt the become password."; \
		false; \
	fi
	@ansible-playbook \
		--connection=local --inventory 127.0.0.1, --limit 127.0.0.1 \
		-e @site.yaml --vault-password-file vault-pass.txt \
		$(extra_args) \
		provision.yaml

clean:
	@vagrant halt || vagrant destroy --force
