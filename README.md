# MyScripts

Here are useful scripts for productivity.

## Test

Test all tasks:

```bash
make test-provision
```

Task partial tasks with specific tags:

```bash
make test-provision run_tags="baseos bash-config"
```

Continue provision without recreating the environment:

```bash
make test-provision continue_last=true
```

## Provide secrets

This is used for provision on local host.

Secrets should be provided via local `site.yaml` file. It contains following
variables whose value are encrypted properly.

- `ansible_become_pass`
- `aliyun_vm_user`

Running a container to encrypt variable values:

```bash
podman run -v $(pwd):/code:Z --rm -it fedora:36
dnf install -y ansible
ansible-vault encrypt_string --name variable_name "value" > /code/site.yaml
exit
```

Create file `vault-pass.txt`, open it, and put vault password there. That will
be used by ansible to decrypt the encrypted password.

## Provision

```bash
make provision-this-machine
# or
make provision-this-machine run_tags="baseos bash-config vim"
```
