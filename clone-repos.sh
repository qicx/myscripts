#!/usr/bin/env bash

if ! which gh >/dev/null 2>&1; then
    curl -LO https://github.com/cli/cli/releases/download/v2.62.0/gh_2.62.0_linux_amd64.rpm
    sudo dnf install ./gh_2.62.0_linux_amd64.rpm
fi

gh auth status || gh auth login

cd $HOME/code

repos="\
konflux tkdchen build-definitions
konflux tkdchen build-service
konflux tkdchen image-controller
konflux tkdchen build-tasks-dockerfiles
konflux tkdchen infra-deployments
. containers skopeo"

while read -r topic namespace repo_name; do
    pushd "$topic"
    [ -e "$repo_name" ] || gh repo clone "git@github.com:${namespace}/${repo_name}"
    popd
done <<<"$repos"
