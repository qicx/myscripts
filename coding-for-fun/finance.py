
import json
import logging
import os
import re
import ssl
import tempfile

from dataclasses import dataclass
from datetime import datetime
from enum import Enum, auto
from html.parser import HTMLParser
from pathlib import Path
from typing import Callable, List, Tuple
from urllib.parse import urlencode
from urllib.request import urlopen, Request

log_file = os.path.join(tempfile.gettempdir(), "finance.log")

logging.basicConfig(
    level=logging.DEBUG, filename=log_file, format="%(levelname)s:%(asctime)s:%(message)s"
)
logger = logging.getLogger(__name__)


class Column(Enum):
    A = auto()
    B = auto()
    C = auto()
    D = auto()
    E = auto()
    F = auto()
    G = auto()
    H = auto()


class FundData:
    def __init__(self):
        self.net_value = ""
        self.data_date = ""


class NetValueIsNotFound(Exception):
    def __init__(self, net_value: str, *args, **kwargs):
        super(*args, **kwargs)
        self._net_value = net_value


def clean_date_in_isoformat(s: str) -> str:
    def _to_isoformat(m: re.Match) -> str:
        return f"{m.group(1)}-{int(m.group(2)):02d}-{int(m.group(3)):02d}"

    if (m := re.match(r"^(\d{4})-(\d{1,2})-(\d{1,2})$", s)) is not None:
        return _to_isoformat(m)

    if (m := re.match(r"^(\d{4})(\d{2})(\d{2})$", s)) is not None:
        return _to_isoformat(m)

    raise ValueError(f"Unknown date format {s}")


def download_web_resource(url: str) -> str:
    """Make HTTP Get request to download content of the resource"""
    ctx = ssl.create_default_context(ssl.Purpose.SERVER_AUTH)
    ctx.options |= 0x4
    with urlopen(url, context=ctx) as resp:
        return resp.read().decode("utf-8")


class SinaFundParser(HTMLParser):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fund_data = FundData()

        self._in_fund_info_block = False
        self._div_count = 0
        self._encounter_net_value = False

    def handle_starttag(self, tag, attrs):
        if tag == "div" and ("id", "fund_info_blk2") in attrs:
            self._div_count += 1
            self._in_fund_info_block = True
            return
        if tag == "div" and self._in_fund_info_block:
            self._div_count += 1

    def handle_endtag(self, tag):
        if tag == "div" and self._in_fund_info_block:
            self._div_count -= 1
            if self._div_count == 0:
               self._in_fund_info_block = False

    def handle_data(self, data):
        if not self._in_fund_info_block:
            return
        value = data.strip()
        if value == "单位净值":
            self._encounter_net_value = True
            return
        if self._encounter_net_value and value:
            self.fund_data.net_value = value
            self._encounter_net_value = False
            return
        if value.startswith("数据日期"):
            # remove prefix "数据日期："
            self.fund_data.data_date = value[5:]
            return


def fetch_fund_net_value(product_code: str) -> Tuple[str, str]:
    net_value_page_url = f"http://finance.sina.com.cn/fund/quotes/{product_code}/bc.shtml"
    net_value_page_html = download_web_resource(net_value_page_url)

    parser = SinaFundParser()
    parser.feed(net_value_page_html)
    data = parser.fund_data

    if not (data.net_value and data.data_date):
        raise NetValueIsNotFound(product_code) 

    return data.net_value, clean_date_in_isoformat(data.data_date)


GUANGDA_PRODUCTS_URLS = {
    "EB1068": "https://www.cebwm.com/wealth/lcxx/lccp14/127554501/index.html"
}


def fetch_guangda_product(product_code: str) -> Tuple[str, str]:
    logger.info("fetch net value for %s", product_code)

    product_info_page = download_web_resource(GUANGDA_PRODUCTS_URLS[product_code])

    net_value, data_date = "", ""

    # html_doc = BeautifulSoup(product_info_page, "html.parser")

    # node = html_doc.find(id="product_main").find(attrs={"class": "hot_yqnh"})
    # net_value = node.find("b").text.strip()
    # data_date = node.find_all("span")
    # print(data_date)

    return net_value, data_date


class CMBChinaNetValueParser(HTMLParser):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fund_data = FundData()

        self._in_product_table = False
        self._td_count = 0

    def handle_starttag(self, tag, attrs):
        if tag == "table" and ("class", "ProductTable") in attrs:
            self._in_product_table = True
            return

        if tag == "td" and self._in_product_table:
            self._td_count += 1
            return

    def handle_endtag(self, tag):
        if tag == "table" and self._in_product_table:
            self._in_product_table = False

    def handle_data(self, data):
        if not self._in_product_table:
            return
        # First td is shown as the table header
        value = data.strip()
        # handle_data is called twice for the td tag.
        # In the first call, the data is the value,
        # but it is an empty string in the second call.
        if self._td_count == 3 and value:
            self.fund_data.net_value = value
        elif self._td_count == 5 and value:
            self.fund_data.data_date = value


def fetch_net_value_from_cmbchina(product_code: str) -> Tuple[str, str]:
    url = (
        "http://www.cmbchina.com/cfweb/Personal/saproductdetail.aspx?"
        f"saaCod=D07&funCod={product_code}&type=prodvalue"
    )
    product_info_page = download_web_resource(url)
    parser = CMBChinaNetValueParser()
    parser.feed(product_info_page)
    data = parser.fund_data
    if not data.net_value:
        raise NetValueIsNotFound(product_code)
    return data.net_value, clean_date_in_isoformat(data.data_date)


def fetch_net_value_from_bcomm(product_code: str) -> Tuple[str, str]:
    url = "https://www.bocommwm.com/BankCommSite/jylc/cn/queryJylcProductDetail.do"
    request_data = urlencode({"c_fundcode": product_code}).encode("ascii")
    with urlopen(Request(url, data=request_data)) as resp:
        product_info = json.loads(resp.read())
    info = product_info["jylcProductBo"]
    return info["f_netvalue"], info["d_cdate"]


@dataclass
class Record:
    row: int
    product_code: str
    product_name: str
    net_value: str
    data_date: str
    finance: str


def get_sheet():
    doc = XSCRIPTCONTEXT.getDocument()
    return doc.Sheets.getByName("最新净值")


def read_source_fund_data():
    """Read fund data from sheet"""
    sheet = get_sheet()
    logger.info("read fund data from sheet %s", sheet.Name)
    row = 0  # The first row is the header
    while True:
        row += 1
        cell_content = sheet[row,Column.A.value-1].String
        if cell_content == "":
            logger.info("No more data. Stop iterating rows.")
            break
        yield Record(
            row=row,
            product_code=sheet[row,Column.A.value-1].String,
            product_name=sheet[row,Column.B.value-1].String,
            net_value=sheet[row,Column.C.value-1].Value,
            data_date=sheet[row,Column.D.value-1].Value,
            finance=sheet[row,Column.E.value-1].String,
        )


def task_fetch_and_update_fund_data(fetch_method: Callable, record: Record, null_date: datetime) -> None:
    sheet = get_sheet()
    net_value, data_date = fetch_method(record.product_code)
    logger.info(
        "fetched for product code %s: net value %s, data date %s",
        record.product_code, net_value, data_date,
    )
    sheet[record.row,Column.C.value-1].Value = net_value
    delta = datetime.fromisoformat(data_date) - null_date
    sheet[record.row,Column.D.value-1].Value = delta.days


def get_null_date() -> datetime:
    doc = XSCRIPTCONTEXT.getDocument()
    number_settings = doc.getNumberFormatSettings()
    null_date = number_settings.getPropertyValue("NullDate")
    return datetime(null_date.Year, null_date.Month, null_date.Day)


def main():
    null_date = get_null_date()
    logger.info("null date: %s", null_date)
    for record in read_source_fund_data():
        if record.finance == "fund":
            fetch_method = fetch_fund_net_value
        elif record.finance == "cmb":
            fetch_method = fetch_net_value_from_cmbchina
        else:
            logger.info("finance %s is not supported yet.", record.finance)
            continue

        try:
            task_fetch_and_update_fund_data(fetch_method, record, null_date)
        except Exception:
            logger.exception("failed to update net value for product code %s", record.product_code)


if __name__ == "__main__":
    main()
