package main

import (
	"fmt"
	"os"

	"github.com/gonvenience/ytbx"
	"github.com/homeport/dyff/pkg/dyff"
)

func main() {
	fromLocation := os.Args[1]
	toLocation := os.Args[2]

	from, to, err := ytbx.LoadFiles(fromLocation, toLocation)
	if err != nil {
		panic(fmt.Errorf("failed to load input files: %w", err))
	}

	report, err := dyff.CompareInputFiles(from, to,
		dyff.IgnoreOrderChanges(false),
		dyff.KubernetesEntityDetection(true))
	if err != nil {
		panic(err)
	}

	report = report.ExcludeRegexp(
		"/spec/tasks/name=.+/taskRef/params/name=bundle/value",
		"/spec/finally/name=.+/taskRef/params/name=bundle/value",
	)

	reportWriter := &dyff.HumanReport{
		Report: report,
		Indent: 2,
		// DoNotInspectCerts:     false,
		NoTableStyle: true,
		OmitHeader:   true,
		// UseGoPatchPaths:       false,
		// MinorChangeThreshold:  -1.1,
		// MultilineContextLines: 3,
		// PrefixMultiline:       false,
	}
	reportWriter.WriteReport(os.Stdout)
}
