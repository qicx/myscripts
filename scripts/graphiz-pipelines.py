import argparse
import yaml
import graphviz

from typing import Final, Iterable

final_node: Final[str] = "final"


class Task:
    def __init__(self, name: str, task_ref: str) -> None:
        self.name = name
        self.run_after: list[str] = []
        self.workspaces: list[dict[str, str]] = []
        self.task_ref = task_ref
        self._ref_count = 0

    def increase_ref(self) -> None:
        self._ref_count += 1

    @property
    def is_leaf(self) -> bool:
        return self._ref_count == 0


def find_leaf_tasks(tasks: dict[str, Task]) -> Iterable[Task]:
    for _, task in tasks.items():
        if task.is_leaf:
            yield task

# TODO: rename this method
def run_after(dot, before: str, this: str) -> None:
    dot.edge(before, this)


def main():
    parser = argparse.ArgumentParser("visible-pipeline")
    parser.add_argument(
        "FILE",
        help="File name of a Tekton pipeline definition file. Generally, this file is written in YAML.",
    )

    args = parser.parse_args()

    dot = graphviz.Digraph("visible-pipeline", directory="output")
    dot.graph_attr["rankdir"] = "LR"

    with open(args.FILE, "r") as f:
        pipeline = yaml.safe_load(f)

    tasks: dict[str, Task] = {}
    task: Task
    pipeline_spec = pipeline["spec"]

    for task_info in pipeline_spec["tasks"]:
        task_name = task_info["name"]
        preceding_tasks = task_info.get("runAfter", [])

        task = Task(name=task_name, task_ref=task_info["taskRef"]["name"])
        task.run_after.extend(preceding_tasks)
        tasks[task_name] = task

        dot.node(task_name, task_name)
        for preceding_task_name in preceding_tasks:
            run_after(dot, preceding_task_name, task_name)
            tasks[preceding_task_name].increase_ref()

        if workspaces := task_info.get("workspaces"):
            task.workspaces.extend(workspaces)

    if final_stage := pipeline_spec.get("finally"):
        dot.node(final_node, final_node, shape="diamond")

        for task in find_leaf_tasks(tasks):
            run_after(dot, task.name, final_node)

        previous_task = final_node
        for task_info in final_stage:
            task_name = task_info["name"]
            dot.node(task_name, task_name)
            run_after(dot, previous_task, task_name)
            previous_task = task_name

    if workspaces := pipeline_spec.get("workspaces"):
        for workspace in workspaces:
            dot.node(workspace["name"], shape="cylinder")

        #  for _, task in tasks.items():
        #      for workspace in task.workspaces:
        #          run_after(dot, task.name, workspace["workspace"])

    dot.render("pipeline", view=True)


if __name__ == "__main__":
    main()
