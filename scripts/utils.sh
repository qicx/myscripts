# quay.io/mytestworkload/build-definitions-bundles-test-builds

image_repo_remove_tags() {
    local -r image_repo=$1
    skopeo list-tags "docker://${image_repo}" |
    jq -r '.Tags[]' | \
    while read -r tag; do
        echo -n "Deleting tag $tag ..."
        skopeo delete "docker://${image_repo}:${tag}"
        echo " done"
    done
}

konflux_build_defs_generate_sample_migration_commit() {
    git branch -D "test-$(git branch --show-current)" || :
    git checkout -b "test-$(git branch --show-current)"
    mkdir task/push-dockerfile/0.1/migrations
    cat <<- EOF >task/push-dockerfile/0.1/migrations/0.1.1.sh
	#!/usr/bin/env bash
	pipeline_file=\$1
	yq -i '
	    (.spec.tasks[] | select(.name == "push-dockerfile") | .params) +=
	    {"name": "CONTAINER_FILE", "value": "./containerfile"}
	' "\$pipeline_file"
EOF
    yq -i '.metadata.labels."app.kubernetes.io/version" |= "0.1.1"' \
        task/push-dockerfile/0.1/push-dockerfile.yaml
    git add \
        task/push-dockerfile/0.1/migrations/0.1.1.sh \
        task/push-dockerfile/0.1/push-dockerfile.yaml
    git commit -m "add migration for testing validation"
}
